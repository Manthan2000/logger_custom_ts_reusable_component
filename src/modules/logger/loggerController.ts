import { LoggerUtils } from "./loggerUtils";
// const LoggerUtils = require('./loggerUtils')
export class LoggerController {
    
    private loggerUtils: LoggerUtils = new LoggerUtils();

    public connect = async(connectionString:any) => {
        try {
            await this.loggerUtils.connect(connectionString) 
        } catch (error) {
            console.log(error);
        }
    }

    public activity = async (userid:any,event:any, sub_event:any, event_response:any) => {
      try {
        await this.loggerUtils.activity(userid, event, sub_event, event_response);
      } catch (error) {
        console.log(error);
      }
    };
  }
