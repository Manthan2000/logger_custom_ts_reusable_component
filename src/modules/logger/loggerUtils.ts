import * as express from 'express';
import mongoose from 'mongoose';
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
var util = require('util');
var encoder = new util.TextEncoder('utf-8');
import { logger } from '../../model/loggerModel';
import * as color from 'cli-color';
export class LoggerUtils {

  public async connect(connectionString:any) {
      try {
           await mongoose.connect(connectionString);
           const data = "info: "+ new Date() +":    "+"Connected to MongoDB"
           console.info(data);
      } catch (error) {
        const data = "error: "+ new Date() +":    "+ color.red("Error connecting to MongoDB:")
        console.info(data, error);
        return data;
      }
  }

  public async activity(userid:any,event:any, sub_event:any, event_response:any) {
    try {
      if (process.env.DATABASETYPE === "mongodb") {
        console.log("Welcolme to Mongodb!!");
        const eventDoc = {
          userid,
          event,
          sub_event,
          event_response
        }

        // Insert the event document into the collection
        const events: any = new logger(eventDoc);
        await events.save();
        const logMsg = "info: " + new Date() + ":    " + color.yellow("Log info is inserted in " + process.env.DATABASETYPE + " database")
        console.info(logMsg);
      }
    } catch (err) {
      console.log(err);
      return err;
    }
  }
}