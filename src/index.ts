import { LoggerController } from './modules/logger/loggerController'
import * as dotenv from 'dotenv';
dotenv.config();

const loggerController = new LoggerController();
export default loggerController;
