"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var loggerController_1 = require("./modules/logger/loggerController");
// require('dotenv').config()
var dotenv = require("dotenv");
dotenv.config();
var loggerController = new loggerController_1.LoggerController();
module.exports = loggerController;
// console.log(process.env.CONNECTION_STRING+"/"+process.env.COLLECTION_NAME,"++++++++");
// loggerController.connect(process.env.CONNECTION_STRING+"/"+process.env.COLLECTION_NAME);
// loggerController.activity('Data1', 'Processing', { file: 'data.csv' })
// loggerController.activity('User', 'Login', { username: 'john12' });   
