"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
var mongoose_1 = require("mongoose");
var loggerSchema = new mongoose_1.Schema({
    event: { type: String, required: true },
    sub_event: { type: String, required: true },
    data: { type: Object, required: true },
    expire_at: { type: Date, default: Date.now, expires: 30 } //expire records after 1 minutes from db
}, {
    versionKey: false,
    timestamps: true
});
var logger = (0, mongoose_1.model)("Event", loggerSchema);
exports.logger = logger;
