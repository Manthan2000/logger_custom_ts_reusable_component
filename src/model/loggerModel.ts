import mongoose, { model, Schema } from "mongoose";

interface loggerinterface {
  userid:String;
  event: string;
  sub_event: string;
  event_response:String;
  expire_at: Date;
}

const loggerSchema = new Schema(
  {
    userid:{type:String,required:true},
    event:{type:String,required:true},
    sub_event:{type:String,required:true},
    event_response:{type:String,required:true},
    expire_at: {type: Date, default: Date.now, expires: process.env.EXPIRE_SEC} //expire records after 1 minutes from db
  },
  {
    versionKey: false,
    timestamps: true
  }
);

const logger = model<loggerinterface>("Event", loggerSchema);
export { logger, loggerinterface };
