import * as express from 'express';
import * as mongoose from 'mongoose';
import * as color from 'cli-color';

const connection = async () => {
  try {
    await mongoose.connect(process.env.CONNECTION_STRING + "/" + process.env.COLLECTION_NAME);
    const data = "info: " + new Date() + ":    " + "Connected to MongoDB"
    console.info(data);
  } catch (error) {
    const data = "error: " + new Date() + ":    " + color.red("Error connecting to MongoDB:")
    console.info(data, error);
    return error
  }
}

export default connection;