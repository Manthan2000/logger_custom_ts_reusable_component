"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const mongoose_1 = require("mongoose");
const loggerSchema = new mongoose_1.Schema({
    userid: { type: String, required: true },
    event: { type: String, required: true },
    sub_event: { type: String, required: true },
    event_response: { type: String, required: true },
    expire_at: { type: Date, default: Date.now, expires: process.env.EXPIRE_SEC } //expire records after 1 minutes from db
}, {
    versionKey: false,
    timestamps: true
});
const logger = (0, mongoose_1.model)("Event", loggerSchema);
exports.logger = logger;
//# sourceMappingURL=loggerModel.js.map