"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const loggerController_1 = require("./modules/logger/loggerController");
const dotenv = require("dotenv");
dotenv.config();
const loggerController = new loggerController_1.LoggerController();
exports.default = loggerController;
//# sourceMappingURL=index.js.map