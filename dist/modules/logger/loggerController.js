"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerController = void 0;
const loggerUtils_1 = require("./loggerUtils");
// const LoggerUtils = require('./loggerUtils')
class LoggerController {
    constructor() {
        this.loggerUtils = new loggerUtils_1.LoggerUtils();
        this.connect = (connectionString) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.loggerUtils.connect(connectionString);
            }
            catch (error) {
                console.log(error);
            }
        });
        this.activity = (userid, event, sub_event, event_response) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.loggerUtils.activity(userid, event, sub_event, event_response);
            }
            catch (error) {
                console.log(error);
            }
        });
    }
}
exports.LoggerController = LoggerController;
//# sourceMappingURL=loggerController.js.map