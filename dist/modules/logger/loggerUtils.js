"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerUtils = void 0;
const express = require("express");
const mongoose_1 = require("mongoose");
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
var util = require('util');
var encoder = new util.TextEncoder('utf-8');
const loggerModel_1 = require("../../model/loggerModel");
const color = require("cli-color");
class LoggerUtils {
    connect(connectionString) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield mongoose_1.default.connect(connectionString);
                const data = "info: " + new Date() + ":    " + "Connected to MongoDB";
                console.info(data);
            }
            catch (error) {
                const data = "error: " + new Date() + ":    " + color.red("Error connecting to MongoDB:");
                console.info(data, error);
                return data;
            }
        });
    }
    activity(userid, event, sub_event, event_response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (process.env.DATABASETYPE === "mongodb") {
                    console.log("Welcolme to Mongodb!!");
                    const eventDoc = {
                        userid,
                        event,
                        sub_event,
                        event_response
                    };
                    // Insert the event document into the collection
                    const events = new loggerModel_1.logger(eventDoc);
                    yield events.save();
                    const logMsg = "info: " + new Date() + ":    " + color.yellow("Log info is inserted in " + process.env.DATABASETYPE + " database");
                    console.info(logMsg);
                }
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.LoggerUtils = LoggerUtils;
//# sourceMappingURL=loggerUtils.js.map